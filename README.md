# Surveyor

### Introduction

A binary single-value marking system for tracking in a multi-dimensional manifold.

That sounds a bit complicated... What does it mean?

Surveyor uses:
 - Binary, single-bit (1/0) markers
 - to track a characteristic (thing)
 - in a 3D grid!

In its intended environment, this means you can create a marker store a single true/false value on every individual block in a Minecraft world!

### Q&A

> **Q:** What sort of stuff is this useful for?
>
> **A:** Tracking whether blocks are man-made or natural, tracking if a block was altered in a certain way, etc.
 
> **Q:** What if I want to have multiple markers?
>
> **A:** You can create as many binary markers as you want, subject to RAM limitations.
 
> **Q:** What if I want to store more than one bit?
>
> **A:** Surveyor has byte-markers built in, but remember: these will use eight times as much RAM!
 
> **Q:** How space-efficient is this?
>
> **A:** VERY. This is the most storage-efficient method possible (for large numbers of values) and has considerations for minimal RAM/hard-disk impact.
 
> **Q:** Can you prove its efficiency?
>
> **A:** Yes. See below. :)

### Developer Usage

// Coming soon, I'm lazy :)


### Efficiency / Capacity

#### Storage

Similar systems often use a key/value system to store data, such as by mapping `block: value`. This is efficient for small numbers - you only store the data you want, and the rest you can ignore.
However, it has a major downside when it comes to storage.

Consider this example:
```json
{
  "97,54,21": 0,
  "100,56,27": 1,
  "100,56,28": 0
}
```
Here, we are storing the block's 3D coordinates and the value. While that value is only a single integer, the key is a multi-character string.

The way Surveyor avoids this is to store the data for EVERY block in a linear order, like so: ```0001001010101111000```

Now clearly, this would be billions of values for an entire world, massive chunks of which would be potentially unused, so instead Surveyor splits the data up.

Firstly, the data is split up into chunks (columns of 16x16x256 blocks), and then again ino chunk sections (cubes of 16x16x16 blocks.)
This means that each individual data section stores exactly 4096 bits (512 bytes) - but that unused data sections do not need to be generated or used.

On top of this, binary data (010101010) can be compressed further, as it is entirely possible that chunk sections will have only one entry in, therefore allowing for further compression.

This means that an entire data section can be rendered to a tiny string, for example: `H4sIAAAAAAAAAGNgGAUjGQAAeHWqsgACAAA`

#### Memory

Of course, we also need to be conscious of system memory, which Surveyor also handles effectively.

One of the limitations of Java is that the smallest unit of memory - even for a primitive value - is a byte. That means that even a boolean, which should effectively take up a single bit, wastes the other seven bits.

Normally, that would mean we would require 4096 bytes (4kb of RAM) to load an entire chunk section's data.

However, due to Surveyor's linear system, we can use only 1/8th of that data (just 512 bytes!)
This is done by using a raster to directly access/edit the individual bits of a byte, using them as token booleans and effectively making this system eight times more efficient than a competitor could be.

On top of this, Surveyor automatically discards unused chunk sections from memory once any changes have been saved.

#### CPU Stress

Due to Surveyor's sectioned load/discard system, one might assume that this puts undue stress on the CPU for loading/saving/checking. Fortunately, almost all of these operations can be carried out in parallel threads and, even if they have to be done in-sync, should have no visible impact on performance whatsoever.

Chunk sections can be pre-loaded asynchronously if their need is pre-determined, meaning CPU-conscious users with excess RAM can pre-load large chunks for fast access. If this process is interrupted or usurped somehow (due to a section being required before the async loader has reached it) it can be loaded in-sync in a matter of milliseconds, due to the incredibly small amount of data stored per section.

On top of this, Surveyor's "c-style" handling of memory (with fixed, linear, primitive-type arrays) and use of the Java native maths library, linear bit calculations should be incredibly fast, especially on newer machines.

In summary, this means that checks should be completely unnoticeable even over large volumes, such as entire chunks. Certainly, much less noticeable than Minecraft's inefficient source!


