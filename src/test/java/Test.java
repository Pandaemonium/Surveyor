import mx.kenzie.survey.logic.BitRaster;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Test {

    public static final BitRaster BIT_RASTER = new BitRaster();
    private static final Map<String, byte[]> map = new HashMap<>();

    public static void main(String[] args) {
        byte a = (byte) 0;
        byte b = (byte) 0;
        a = BIT_RASTER.write(a, 0, true);
        a = BIT_RASTER.write(a, 1, false);
        a = BIT_RASTER.write(a, 2, false);
        a = BIT_RASTER.write(a, 3, true);
        System.out.println(Arrays.toString(BIT_RASTER.read(a, 0, 1, 2, 3)));
        b = BIT_RASTER.write(b, true, 0, 2, 4, 7);
        b = BIT_RASTER.write(b, false, 1, 3, 5, 6);
        System.out.println(Arrays.toString(BIT_RASTER.read(b)));
        map.put("hi!", new byte[]{0, 1, 2});
        byte[] bytes = map.get("hi!");
        bytes[0] = 5;
        System.out.println(Arrays.toString(map.get("hi!")));
    }

}
