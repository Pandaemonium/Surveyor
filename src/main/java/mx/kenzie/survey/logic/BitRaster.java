package mx.kenzie.survey.logic;

public class BitRaster {

    public BitRaster() {
    }

    public boolean read(final byte bi, int i) {
        return (bi >>> i & 1) == 1;
    }

    public boolean[] read(byte bi, int... ints) {
        boolean[] boos = new boolean[ints.length];
        int n = 0;
        for (int i : ints) {
            boos[n] = (bi >>> i & 1) == 1;
            n++;
        }
        return boos;
    }

    public boolean[] read(byte bi) {
        boolean[] boos = new boolean[8];
        for (int i = 0; i < 8; i++) {
            boos[i] = (bi >>> i & 1) == 1;
        }
        return boos;
    }

    public byte write(byte bi, int i, boolean boo) {
        if (boo) bi |= 1 << i;
        else bi &= ~(1 << i);
        return bi;
    }

    public byte write(byte bi, boolean boo, int... ints) {
        if (boo) {
            for (int i : ints) {
                bi |= 1 << i;
            }
        } else {
            for (int i : ints) {
                bi &= ~(1 << i);
            }
        }
        return bi;
    }

    public byte write(byte bi, boolean boo) {
        if (boo) {
            for (int i = 0; i < 8; i++) {
                bi |= 1 << i;
            }
        } else {
            for (int i = 0; i < 8; i++) {
                bi &= ~(1 << i);
            }
        }
        return bi;
    }

    public int last(int i, int sec) {
        int n = (int) Math.floor(i / (double) sec);
        return (i - n * sec);
    }

}
