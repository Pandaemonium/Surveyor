package mx.kenzie.survey.logic;

import mx.kenzie.survey.Surveyor;
import mx.kenzie.survey.api.BinaryMarker;
import org.bukkit.NamespacedKey;
import org.jetbrains.annotations.NotNull;

public class PlacementMarker extends BinaryMarker {

    public PlacementMarker() {
        super(450, 180, 120);
    }

    @Override
    public @NotNull NamespacedKey getName() {
        return new NamespacedKey(Surveyor.<Surveyor>getInstance(), "man_made");
    }
}
