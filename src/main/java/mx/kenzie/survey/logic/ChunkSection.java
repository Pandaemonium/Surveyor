package mx.kenzie.survey.logic;

import com.moderocky.mask.api.MagicList;
import mx.kenzie.survey.Surveyor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class ChunkSection {

    private final Chunk chunk;
    private final int section;

    public ChunkSection(Location location) {
        chunk = location.getChunk();
        section = Math.min(location.getBlockY() >> 4, 15);
    }

    public ChunkSection(Chunk chunk, int pos) {
        this.chunk = chunk;
        this.section = pos;
    }

    public Collection<Block> getBlocks() {
        List<Block> blocks = new ArrayList<>();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = section * 16; y < (section * 16) + 16; y++) {
                    blocks.add(chunk.getBlock(x, y, z));
                }
            }
        }
        return blocks;
    }

    public int getBlockPos(Block block) {
        int x = block.getX() - (chunk.getX() * 16);
        int y = Surveyor.BIT_RASTER.last(block.getY(), 16);
        int z = block.getZ() - (chunk.getZ() * 16);
        return x + (z * 16) + (y * 256);
    }

    public Block getBlock(int pos) {
        int q = pos;
        int y = (int) Math.floor(q/256.0F);
        q = Surveyor.BIT_RASTER.last(q, 256);
        int z = (int) Math.floor(q/16.0F);
        int x = Surveyor.BIT_RASTER.last(q, 16);
        return chunk.getBlock(x, y, z);
    }

    public World getWorld() {
        return chunk.getWorld();
    }

    public Chunk getChunk() {
        return chunk;
    }

    public int getSection() {
        return section;
    }

    public String getKey() {
        return chunk.getX() + "_" + chunk.getZ() + "_" + section;
    }

    public File getFile(File path) {
        return new File(path, getWorld().getName() + "/" + chunk.getX() + "_" + chunk.getZ() + "/" + section + ".sec");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChunkSection)) return false;
        ChunkSection that = (ChunkSection) o;
        return section == that.section &&
                Objects.equals(chunk, that.chunk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chunk, section);
    }

    public static ChunkSection[] getSections(Chunk chunk) {
        MagicList<ChunkSection> sections = new MagicList<>();
        for (int i = 0; i < 16; i++) {
            sections.add(new ChunkSection(chunk, i));
        }
        return sections.toArray(new ChunkSection[0]);
    }

}
