package mx.kenzie.survey.logic;

import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public interface Marker<T> {

    @NotNull NamespacedKey getName();

    default @NotNull File getPath() {
        return new File("plugins/Surveyor/" + getName().getNamespace() + "/" + getName().getKey() + "/");
    }

    T get(Location location);

    void set(Location location, T val);

    void scheduleSave();

    void save();

}
