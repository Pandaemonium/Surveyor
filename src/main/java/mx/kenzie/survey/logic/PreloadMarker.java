package mx.kenzie.survey.logic;

import org.bukkit.Location;

public interface PreloadMarker<T> extends Marker<T> {

    void preload(Location location);

    void unload(Location location);

    boolean isLoaded(Location location);

}
