package mx.kenzie.survey;

import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.template.BukkitPlugin;
import com.moderocky.mask.template.CompleteListener;
import mx.kenzie.survey.config.SurveyorConfig;
import mx.kenzie.survey.logic.*;
import org.bstats.bukkit.Metrics;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@SuppressWarnings("unused")
public class Surveyor extends BukkitPlugin {

    public static final BitRaster BIT_RASTER = new BitRaster();
    public static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(2);
    public static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(4);
    public static final Map<NamespacedKey, Marker<?>> MARKERS = new HashMap<>();
    public final Natural NATURAL = new Natural();
    public final SurveyorConfig config = new SurveyorConfig();
    private final Metrics metrics = new Metrics(this, 7942);
    private final PreloadMarker<Boolean> marker = new PlacementMarker();

    public static Natural getNature() {
        return Surveyor.<Surveyor>getInstance().getNatureInt();
    }

    private Natural getNatureInt() {
        return new Natural();
    }

    public static void registerMarker(Marker<?> marker) {
        Surveyor.MARKERS.put(marker.getName(), marker);
    }

    public static <T> Marker<T> getMarker(NamespacedKey key) {
        return (Marker<T>) Surveyor.MARKERS.get(key);
    }

    @Override
    public void startup() {
        config.load();
        listen();
    }

    public boolean isNatural(Location location) {
        return marker.get(location);
    }

    @Override
    public void disable() {
        marker.save();
        MARKERS.clear();
    }

    private void listen() {
        registerMarker(marker);
        if (config.trackPlayerPlacedBlocks) {
            register(new CompleteListener() {
                @EventHandler(priority = EventPriority.HIGHEST)
                public void event(BlockPlaceEvent event) {
                    if (event.isCancelled()) return;
                    marker.set(event.getBlock().getLocation(), event.getBlock().getType() != Material.AIR);
                    marker.scheduleSave();
                }
            });
            register(new CompleteListener() {
                @EventHandler(priority = EventPriority.HIGHEST)
                public void event(BlockBreakEvent event) {
                    if (event.isCancelled()) return;
                    marker.set(event.getBlock().getLocation(), false);
                    marker.scheduleSave();
                }
            });
            register(new CompleteListener() {
                @EventHandler(priority = EventPriority.HIGHEST)
                public void event(BlockFormEvent event) {
                    if (event.isCancelled()) return;
                    marker.set(event.getBlock().getLocation(), false);
                    marker.scheduleSave();
                }
            });
        }
    }

    public class Natural {

        private Natural() {
        }

        public boolean isEnabled() {
            return config.trackPlayerPlacedBlocks;
        }

        public boolean isNatural(Block block) {
            return marker.get(block.getLocation());
        }

        public boolean isNatural(Location location) {
            return marker.get(location);
        }

        public void setNatural(Location location, boolean boo) {
            marker.set(location, boo);
        }

        public boolean isLoaded(Location location) {
            return marker.isLoaded(location);
        }

        public void load(Location location) {
            marker.preload(location);
        }

        public Block[] getNaturalBlocks(ChunkSection section) {
            MagicList<Block> blocks = new MagicList<>(section.getBlocks());
            blocks.removeIf(block -> !isNatural(block));
            return blocks.toArray(new Block[0]);
        }

        public Block[] getUnnaturalBlocks(ChunkSection section) {
            MagicList<Block> blocks = new MagicList<>(section.getBlocks());
            blocks.removeIf(this::isNatural);
            return blocks.toArray(new Block[0]);
        }

        public Block[] getNaturalBlocks(Chunk chunk) {
            MagicList<Block> blocks = new MagicList<>();
            for (int i = 0; i < 16; i++) {
                blocks.addAll(getNaturalBlocks(new ChunkSection(chunk, i)));
            }
            return blocks.toArray(new Block[0]);
        }

        public Block[] getUnnaturalBlocks(Chunk chunk) {
            MagicList<Block> blocks = new MagicList<>();
            for (int i = 0; i < 16; i++) {
                blocks.addAll(getUnnaturalBlocks(new ChunkSection(chunk, i)));
            }
            return blocks.toArray(new Block[0]);
        }

        public boolean hasNaturalBlocks(ChunkSection section) {
            for (Block block : section.getBlocks()) {
                if (isNatural(block)) return true;
            }
            return false;
        }

        public boolean hasNaturalBlocks(Chunk chunk) {
            for (ChunkSection section : ChunkSection.getSections(chunk)) {
                if (hasNaturalBlocks(section)) return true;
            }
            return false;
        }

        public ChunkSection getSection(Block block) {
            return new ChunkSection(block.getLocation());
        }

        public ChunkSection getSection(Location location) {
            return new ChunkSection(location);
        }

        public Block getBlock(ChunkSection section, int position) {
            return section.getBlock(position);
        }

        public int getBlockPosition(ChunkSection section, Block block) {
            return section.getBlockPos(block);
        }

    }

}
