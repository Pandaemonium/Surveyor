package mx.kenzie.survey.api;

import com.moderocky.mask.api.Compressive;
import com.moderocky.mask.internal.utility.FileManager;
import mx.kenzie.survey.Surveyor;
import mx.kenzie.survey.logic.ChunkSection;
import mx.kenzie.survey.logic.PreloadMarker;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public abstract class BinaryMarker implements PreloadMarker<Boolean>, Compressive {

    private final Map<ChunkSection, byte[]> byteMap = new HashMap<>();
    private final List<ChunkSection> saveList = new ArrayList<>();

    public BinaryMarker(int initialDelay, int savePeriod, int unloadPeriod) {
        Surveyor.SCHEDULER.scheduleAtFixedRate(this::scheduleSave, initialDelay, savePeriod, TimeUnit.SECONDS);
        Surveyor.SCHEDULER.scheduleAtFixedRate(this::unloadSections, initialDelay, unloadPeriod, TimeUnit.SECONDS);
    }

    @Override
    public void preload(Location location) {
        ChunkSection section = new ChunkSection(location);
        if (byteMap.containsKey(section)) return;
        loadSectionAsync(section);
    }

    @Override
    public void unload(Location location) {
        ChunkSection section = new ChunkSection(location);
        if (saveList.contains(section)) return;
        byteMap.remove(section);
    }

    @Override
    public boolean isLoaded(Location location) {
        ChunkSection section = new ChunkSection(location);
        return (byteMap.containsKey(section));
    }

    @Override
    public abstract @NotNull NamespacedKey getName();

    public boolean ignoreAir() {
        return !Surveyor.<Surveyor>getInstance().config.markAir;
    }

    @Override
    public Boolean get(Location location) {
        ChunkSection section = new ChunkSection(location);
        if (!byteMap.containsKey(section)) loadSection(section);
        int i = section.getBlockPos(location.getBlock());
        int ref = (int) Math.floor(i / 8.0D);
        return Surveyor.BIT_RASTER.read(byteMap.get(section)[ref], Surveyor.BIT_RASTER.last(i, 8));
    }

    @Override
    public void set(Location location, Boolean boo) {
        ChunkSection section = new ChunkSection(location);
        if (!byteMap.containsKey(section)) loadSection(section);
        int i = section.getBlockPos(location.getBlock());
        int ref = (int) Math.floor(i / 8.0D);
        int slot = Surveyor.BIT_RASTER.last(i, 8);
        final byte[] bytes = byteMap.get(section);
        final byte bi = bytes[ref];
        bytes[ref] = Surveyor.BIT_RASTER.write(bi, slot, boo);
        scheduleSave(section);
    }

    public void unloadSections() {
        for (ChunkSection section : byteMap.keySet()) {
            if (saveList.contains(section)) continue;
            byteMap.remove(section);
        }
    }

    public void scheduleSave(ChunkSection section) {
        if (saveList.contains(section)) return;
        saveList.add(section);
    }

    @Override
    public void scheduleSave() {
        if (!saveList.isEmpty())
            Surveyor.THREAD_POOL.execute(this::save);
    }

    @Override
    public synchronized void save() {
        for (ChunkSection section : new ArrayList<>(saveList)) {
            byte[] bytes = byteMap.getOrDefault(section, new byte[512]);
            File file = section.getFile(getPath());
            FileManager.putIfAbsent(file);
            FileManager.writeBytes(file, zip(new String(bytes)));
            saveList.remove(section);
        }
    }

    private void loadSectionAsync(ChunkSection section) {
        Surveyor.THREAD_POOL.execute(() -> loadSection(section));
    }

    private void loadSection(ChunkSection section) {
        if (byteMap.containsKey(section)) return;
        final byte[] bytes = new byte[512];
        try {
            final File file = section.getFile(getPath());
            FileManager.putIfAbsent(file);
            final byte[] bclone = unzip(FileManager.readBytes(file)).getBytes();
            System.arraycopy(bclone, 0, bytes, 0, Math.min(512, bclone.length));
        } catch (Throwable ignore) {
        }
        byteMap.putIfAbsent(section, bytes);
    }
}
