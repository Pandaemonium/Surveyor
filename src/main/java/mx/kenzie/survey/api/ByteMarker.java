package mx.kenzie.survey.api;

import com.moderocky.mask.api.Compressive;
import com.moderocky.mask.internal.utility.FileManager;
import mx.kenzie.survey.Surveyor;
import mx.kenzie.survey.logic.ChunkSection;
import mx.kenzie.survey.logic.PreloadMarker;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public abstract class ByteMarker implements PreloadMarker<Byte>, Compressive {

    private final Map<ChunkSection, byte[]> byteMap = new HashMap<>();
    private final List<ChunkSection> saveList = new ArrayList<>();

    public ByteMarker(int initialDelay, int savePeriod, int unloadPeriod) {
        Surveyor.SCHEDULER.scheduleAtFixedRate(this::scheduleSave, initialDelay, savePeriod, TimeUnit.SECONDS);
        Surveyor.SCHEDULER.scheduleAtFixedRate(this::unloadSections, initialDelay, unloadPeriod, TimeUnit.SECONDS);
    }

    @Override
    public void preload(Location location) {
        ChunkSection section = new ChunkSection(location);
        if (byteMap.containsKey(section)) return;
        loadSectionAsync(section);
    }

    @Override
    public void unload(Location location) {
        ChunkSection section = new ChunkSection(location);
        if (saveList.contains(section)) return;
        byteMap.remove(section);
    }

    @Override
    public boolean isLoaded(Location location) {
        ChunkSection section = new ChunkSection(location);
        return (byteMap.containsKey(section));
    }

    @Override
    public abstract @NotNull NamespacedKey getName();

    public boolean ignoreAir() {
        return !Surveyor.<Surveyor>getInstance().config.markAir;
    }

    @Override
    public Byte get(Location location) {
        ChunkSection section = new ChunkSection(location);
        if (!byteMap.containsKey(section)) loadSection(section);
        int i = section.getBlockPos(location.getBlock());
        return byteMap.get(section)[i];
    }

    @Override
    public void set(Location location, Byte bi) {
        ChunkSection section = new ChunkSection(location);
        if (!byteMap.containsKey(section)) loadSection(section);
        int i = section.getBlockPos(location.getBlock());
        final byte[] bytes = byteMap.get(section);
        bytes[i] = bi;
        scheduleSave(section);
    }

    public void unloadSections() {
        for (ChunkSection section : byteMap.keySet()) {
            if (saveList.contains(section)) continue;
            byteMap.remove(section);
        }
    }

    public void scheduleSave(ChunkSection section) {
        if (saveList.contains(section)) return;
        saveList.add(section);
    }

    @Override
    public void scheduleSave() {
        if (!saveList.isEmpty())
            Surveyor.THREAD_POOL.execute(this::save);
    }

    @Override
    public synchronized void save() {
        for (ChunkSection section : new ArrayList<>(saveList)) {
            byte[] bytes = byteMap.getOrDefault(section, new byte[512]);
            File file = section.getFile(getPath());
            FileManager.putIfAbsent(file);
            FileManager.writeBytes(file, zip(new String(bytes)));
            saveList.remove(section);
        }
    }

    private void loadSectionAsync(ChunkSection section) {
        Surveyor.THREAD_POOL.execute(() -> loadSection(section));
    }

    private void loadSection(ChunkSection section) {
        if (byteMap.containsKey(section)) return;
        final byte[] bytes = new byte[4096];
        try {
            final File file = section.getFile(getPath());
            FileManager.putIfAbsent(file);
            final byte[] bclone = unzip(FileManager.readBytes(file)).getBytes();
            System.arraycopy(bclone, 0, bytes, 0, Math.min(4096, bclone.length));
        } catch (Throwable ignore) {
        }
        byteMap.putIfAbsent(section, bytes);
    }
}
