package mx.kenzie.survey.config;

import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.template.Config;
import org.jetbrains.annotations.NotNull;

public class SurveyorConfig implements Config {

    @Configurable
    public boolean markAir = false;

    @Configurable
    public boolean trackPlayerPlacedBlocks = true;

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/Surveyor/";
    }

    @Override
    public @NotNull String getFileName() {
        return "config.yml";
    }

}
